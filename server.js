// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');
var bodyParser = require('body-parser');
var app        = express();
var morgan     = require('morgan');

// configure app
app.use(morgan('dev')); // log requests to the console

// configure body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port     = process.env.PORT || 8080; // set our port

// ROUTES FOR OUR API
// =============================================================================

// create our router
var router = express.Router();

// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging
	console.log('Something is happening.');
	next();
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
	res.json({ message: 'welcom to point' });	
});

router.post('/point/add', function(req, res) {
    var customerId = req.body.customerId;
    var pointAmount = req.body.pointAmount;
	res.json({
		status: "ok",
		code: 200,
		messages: []
	});	
});

router.post('/point/redeem', function(req, res) {
	var customerId = req.body.customerId;
    var pointAmount = req.body.pointAmount;
	res.json({
		status: "ok",
		code: 200,
		messages: []
	});		
});

router.post('/point/cancel', function(req, res) {
	var customerId = req.body.customerId;
    var pointAmount = req.body.pointAmount;
	res.json({
		status: "ok",
		code: 200,
		messages: []
	});	
});

// REGISTER OUR ROUTES -------------------------------
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
